const express = require('express');
const path = require('path');
const app = express();
const compression = require('compression');
const mongoConnect = require('./util/database').mongoConnect;
const dotenv = require('dotenv');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const mailController = require('./controllers/mailController');
// const bodyParser = require('body-parser');

// setting up ejs view engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

//
app.use(compression());

// Middleware to serve static file such as image, css, js
app.use(express.static(path.join(__dirname, 'public')));

// Parsing the urlencoded or UTF-8 encoded incoming request body
app.use(express.urlencoded({ extended: false }));

// configuring dotenv
dotenv.config({ path: './config/config.env' });

// for route purpose
const homeRoutes = require('./routes/home-routes');
const adminRoutes = require('./routes/admin-routes');
const mailRoutes = require('./routes/mail-routes');
const authRoutes = require('./routes/auth-routes');

// defining store configuration for mongo session to store in a mongo database
const store = MongoDBStore({
  uri: 'mongodb+srv://pema:atlasmongo2021@cluster0.9wgye.mongodb.net/portfolio',
  collection: 'session',
});

// Initializing session
app.use(
  session({
    secret: 'my secret',
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);

// Sending data for our views
app.use((req, res, next) => {
  res.locals.isLoggedIn = req.session.isLoggedIn;
  next();
});

// Routes to run
app.use(homeRoutes);
app.use('/admin', adminRoutes);
app.use('/mail', mailRoutes);
app.use('/auth', authRoutes);

// Connecting the mongodb and running the express server
mongoConnect(() => {
  app.listen(process.env.PORT, () => {
    console.log('connected.....');
  });
});
