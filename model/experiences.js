const getDb = require('../util/database').getDb;
const mongodb = require('mongodb');

class Experiences {
  constructor(degree, beginningYear, endYear, address, jobDetails, id) {
    this.degree = degree;
    this.beginningYear = beginningYear;
    this.endYear = endYear;
    this.address = address;
    this.jobDetails = jobDetails;
    this._id = id ? new mongodb.ObjectId(id) : null;
  }

  saveExperiences() {
    const db = getDb();
    let dbOp;
    if (this._id) {
      dbOp = db.collection('experiences').updateOne(
        { _id: this._id },
        {
          $set: {
            degree: this.degree,
            beginningYear: this.beginningYear,
            endYear: this.endYear,
            address: this.address,
            jobDetails: { description: this.jobDetails },
          },
        }
      );
    } else {
      dbOp = db.collection('experiences').insertOne({
        degree: this.degree,
        beginningYear: this.beginningYear,
        endYear: this.endYear,
        address: this.address,
        jobDetails: { description: this.jobDetails },
      });
    }
    return dbOp
      .then((result) => {
        console.log(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  static fetchExperiences() {
    const db = getDb();
    return db
      .collection('experiences')
      .find()
      .toArray()
      .then((experiences) => {
        return experiences;
      })
      .catch((err) => {
        console.log(err);
      });
  }

  // Find by id
  static findById(id) {
    const db = getDb();
    return db
      .collection('experiences')
      .find({ _id: new mongodb.ObjectId(id) })
      .next()
      .then((experience) => {
        return experience;
      })
      .catch();
  }

  // Delete by id
  static deleteById(id) {
    const db = getDb();
    return db
      .collection('experiences')
      .deleteOne({ _id: new mongodb.ObjectId(id) });
  }
}
module.exports = Experiences;
