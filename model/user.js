const getDb = require('../util/database').getDb;
class User {
  constructor(email, password) {
    this.email = email;
    this.password = password;
  }
  saveUser() {
    const db = getDb();
    db.collection('user').insertOne(this);
  }
  static findByEmail(email) {
    const db = getDb();
    return db
      .collection('user')
      .find({ email: email })
      .next()
      .then((user) => {
        return user;
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

module.exports = User;
