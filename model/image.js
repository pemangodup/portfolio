const getDb = require('../util/database').getDb;
class Image {
  constructor() {}
  saveImage(finalImg) {
    const db = getDb();
    return db
      .collection('image')
      .insertOne(finalImg)
      .then((img) => {
        return img;
      })
      .catch((err) => {
        console.log(err);
      });
  }

  static fetchImage() {
    const db = getDb();
    return db
      .collection('image')
      .find()
      .toArray()
      .then((image) => {
        return image;
      })
      .catch((err) => {
        console.log(err);
      });
  }
  static updateImage(imgId, finalImg) {
    const db = getDb();
    db.collection('image').updateOne({ _id: imgId }, { $set: finalImg });
  }
}

module.exports = Image;
