const getDb = require('../util/database').getDb;
const mongodb = require('mongodb');

class About {
  constructor(
    about,
    jobTitle,
    aboutJobTitle,
    name,
    jobDetails,
    phone,
    titles,
    city,
    degree,
    address,
    email,
    id
  ) {
    this.about = about;
    this.jobtitle = jobTitle;
    this.aboutJobTitle = aboutJobTitle;
    this.name = name;
    this.jobDetails = jobDetails;
    this.phone = phone;
    this.titles = titles;
    this.city = city;
    this.degree = degree;
    this.address = address;
    this.email = email;
    this._id = id ? new mongodb.ObjectId(id) : null;
  }
  saveAbout() {
    const db = getDb();
    let dbOp;
    if (this._id) {
      dbOp = db
        .collection('about')
        .updateOne({ _id: this._id }, { $set: this });
    } else {
      dbOp = db.collection('about').insertOne(this);
    }
    return dbOp
      .then((result) => {
        console.log(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  static fetchAbout() {
    const db = getDb();
    return db
      .collection('about')
      .find()
      .toArray()
      .then((about) => {
        return about;
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
module.exports = About;
