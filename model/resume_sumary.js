const getDb = require('../util/database').getDb;
const mongodb = require('mongodb');
class ResumeSummary {
  constructor(resume, id) {
    this.resume = resume;
    this._id = id ? new mongodb.ObjectId(id) : null;
  }
  saveResume() {
    const db = getDb();
    if (_db) {
      db.collection('resume').insertOne(this);
    }
  }
  updateResume() {
    const db = getDb();
    db.collection('resume').updateOne(
      {
        _id: this._id,
      },
      { $set: this }
    );
  }
}
