const getDb = require('../util/database').getDb;
const mongodb = require('mongodb');
class Education {
  constructor(degree, beginningYear, endYear, location, details, id) {
    this.degree = degree;
    this.beginningYear = beginningYear;
    this.endYear = endYear;
    this.location = location;
    this.details = details;
    this._id = id ? new mongodb.ObjectId(id) : null;
  }

  saveEducation() {
    const db = getDb();
    let dbOp;
    if (this._id) {
      console.log(` Inserted Here: ${this._id}`);
      dbOp = db
        .collection('education')
        .updateOne({ _id: this._id }, { $set: this });
    } else {
      dbOp = db.collection('education').insertOne(this);
    }
    return dbOp
      .then((result) => {
        console.log(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  static fetchEducation() {
    const db = getDb();
    return db
      .collection('education')
      .find()
      .toArray()
      .then((education) => {
        return education;
      })
      .catch((err) => {
        console.log(err);
      });
  }

  // Find the education by id
  static findById(id) {
    const db = getDb();
    return db
      .collection('education')
      .find({ _id: new mongodb.ObjectId(id) })
      .next()
      .then((education) => {
        return education;
      })
      .catch((err) => {
        console.log(err);
      });
  }

  // Delete by id
  static deleteById(id) {
    const db = getDb();
    return db
      .collection('education')
      .deleteOne({ _id: new mongodb.ObjectId(id) });
  }
}

module.exports = Education;
