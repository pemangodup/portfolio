const About = require('../model/about');
const Education = require('../model/education');
const Experiences = require('../model/experiences');
const Image = require('../model/image');

// Fetching and sending all the details of about section from database to frontend
exports.getHome = (req, res, next) => {
  About.fetchAbout()
    .then((about) => {
      Education.fetchEducation()
        .then((education) => {
          Experiences.fetchExperiences()
            .then((experiences) => {
              Image.fetchImage()
                .then((image) => {
                  res.render('../views/index.ejs', {
                    abt: about,
                    edu: education,
                    exp: experiences,
                    img: image,
                  });
                })
                .catch((err) => {
                  console.log(err);
                });
            })
            .catch((err) => {
              console.log(err);
            });
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
};
// exports.postAddProduct = (req, res, next) => {
//   const title = req.body.about;
//   const img = req.body.img;
//   const jobTitle = req.body.jobTitle;
//   const jobDetails = req.body.jobDetails;
//   const phone = req.body.phone;
//   const city = req.body.city;
//   const degree = req.body.degree;
//   const email = req.body.email;
//   const about = new About(
//     title,
//     img,
//     jobTitle,
//     jobDetails,
//     phone,
//     city,
//     degree,
//     email
//   );
//   about.saveAbout();
// };
