const About = require('../model/about');
const Education = require('../model/education');
const fs = require('fs');
const Image = require('../model/image');
const getDb = require('../util/database').getDb;
const Experiences = require('../model/experiences');
const { title } = require('process');
const { uploadFile } = require('../util/s3');

// Getting admin home page
exports.getIndex = (req, res, next) => {
  res.render('../views/admin/admin.ejs', {
    pageTitle: 'Admin',
    path: '/',
  });
};

// Getting the admin about form page
exports.getAbout = (req, res, next) => {
  About.fetchAbout()
    .then((about) => {
      if (about.length == 1) {
        res.render('../views/admin/admin-about.ejs', {
          abt: about,
          pageTitle: 'Admin Panel',
          path: '/admin/about',
          editing: 'editMode',
        });
      } else {
        res.render('../views/admin/admin-about.ejs', {
          pageTitle: 'Admin Panel',
          path: '/admin/about',
          editing: null,
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

// Saving about section
exports.saveAbout = (req, res, next) => {
  const abt = req.body.about;
  const jobTitle = req.body.jobtitle;
  const aboutJobTitle = req.body.aboutJobTitle;
  const name = req.body.name;
  const jobDetails = req.body.jobdetails;
  const phone = req.body.phone;
  const titles = req.body.titles;
  const city = req.body.city;
  const degree = req.body.degree;
  const address = req.body.address;
  const email = req.body.email;
  const id = req.body.aboutId;
  console.log(`This is my id hehe: ${id}`);
  const about = new About(
    abt,
    jobTitle,
    aboutJobTitle,
    name,
    jobDetails,
    phone,
    titles,
    city,
    degree,
    address,
    email,
    id
  );

  about
    .saveAbout()
    .then((about) => {
      console.log('Saved to database.');
      res.redirect('/');
    })
    .catch((err) => {
      console.log(err);
    });
};
// End of about section.....

// Now it is for education section......
exports.getEducation = (req, res, next) => {
  Education.fetchEducation()
    .then((education) => {
      res.render('../views/admin/admin-education.ejs', {
        pageTitle: 'Education',
        path: '/admin/education',
        editing: null,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.saveEducation = (req, res, next) => {
  const degree = req.body.degree;
  const begyear = req.body.beginningYear;
  const endYear = req.body.endYear;
  const address = req.body.address;
  const details = req.body.details;

  const education = new Education(degree, begyear, endYear, address, details);

  education
    .saveEducation()
    .then((details) => {
      console.log('Saved education to database.');
      res.redirect('/');
    })
    .catch((err) => {
      console.log(err);
    });
};

// Getting the experiences data and rendering the ejs page
exports.getExperiences = (req, res, next) => {
  Experiences.fetchExperiences()
    .then((experiences) => {
      res.render('../views/admin/admin-experiences.ejs', {
        pageTitle: 'Experiences',
        path: '/admin/experiences',
        editing: null,
        exp: experiences,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

// Saving experiences to the database
exports.postExperiences = (req, res, next) => {
  const degree = req.body.degree;
  const begyear = req.body.beginningYear;
  const endYear = req.body.endYear;
  const address = req.body.address;
  const jobDetails = req.body.jobDetails;

  const experiences = new Experiences(
    degree,
    begyear,
    endYear,
    address,
    jobDetails
  );

  experiences
    .saveExperiences()
    .then((address) => {
      console.log('Saved experiences to database.');
      res.redirect('/');
    })
    .catch((err) => {
      console.log(err);
    });
};

// Getting edit experiences ejs page with experinece data
exports.getEditExperiences = (req, res, next) => {
  const id = req.params.id;
  Experiences.findById(id)
    .then((experience) => {
      res.render('../views/admin/admin-experiences.ejs', {
        exp: experience,
        path: '/admin/getEditEducation',
        editing: 'editMode',
        pageTitle: 'Update Experiences',
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

// Post edit experiences
exports.postEditExperiences = (req, res, next) => {
  const degree = req.body.degree;
  const begyear = req.body.beginningYear;
  const endYear = req.body.endYear;
  const address = req.body.address;
  const jobDetails = req.body.jobDetails;
  const id = req.body.eduId;

  const experiences = new Experiences(
    degree,
    begyear,
    endYear,
    address,
    jobDetails,
    id
  );
  experiences
    .saveExperiences()
    .then((experiences) => {
      console.log('Updated education successfully.');
      res.redirect('/admin/update/all');
    })
    .catch((err) => {
      console.log(err);
    });
};

//Delete experiences by id
exports.deleteExperiences = (req, res, next) => {
  const id = req.params.id;
  Experiences.deleteById(id)
    .then((exp) => {
      console.log(exp);
      res.redirect('/admin/update/all');
    })
    .catch((err) => {
      console.log(err);
    });
};

// Getting the image update page
exports.getImagePage = (req, res, next) => {
  Image.fetchImage()
    .then((image) => {
      res.render('../views/admin/uploadImg', {
        pageTitle: 'Upload Image',
        path: '/admin/uploadImg',
        img: image,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

// Posting the image data in to the database
exports.postImage = async (req, res, next) => {
  const file = req.file;
  console.log(file);
  const result = await uploadFile(file);
  console.log(result);
  res.redirect('/admin');
};

// Get education update page
exports.getUpdate = (req, res, next) => {
  Experiences.fetchExperiences()
    .then((details) => {
      Education.fetchEducation()
        .then((education) => {
          res.render('../views/admin/update.ejs', {
            pageTitle: 'Update',
            path: '/admin/update',
            exp: details,
            edu: education,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
};

// Get update education
exports.getUpdateEducation = (req, res, next) => {
  const id = req.params.id;
  Education.findById(id)
    .then((education) => {
      res.render('../views/admin/admin-education.ejs', {
        edu: education,
        editing: 'editing',
        pageTitle: 'Update Education',
        path: '/admin/updateEducation',
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

// Post edit education
exports.postEditEducation = (req, res, next) => {
  const degree = req.body.degree;
  const begyear = req.body.beginningYear;
  const endYear = req.body.endYear;
  const address = req.body.address;
  const details = req.body.details;
  const id = req.body.id;
  const education = new Education(
    degree,
    begyear,
    endYear,
    address,
    details,
    id
  );

  education
    .saveEducation()
    .then((education) => {
      console.log('Edited education saved.');
      res.redirect('/admin/update/all');
    })
    .catch((err) => {
      console.log(err);
    });
};

// Delete education by id
exports.deleteEducation = (req, res, next) => {
  const id = req.body.eduId;
  Education.deleteById(id)
    .then((education) => {
      res.redirect('/admin/update/all');
    })
    .catch((err) => {
      console.log(err);
    });
};

// Logout from session
exports.logout = (req, res, next) => {
  req.session.destroy((err) => {
    console.log(err);
    res.redirect('/admin');
  });
};
