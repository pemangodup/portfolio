const User = require('../model/user');
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
exports.getLogin = (req, res, next) => {
  res.render('../views/auth/login.ejs', {
    pageTitle: 'Login',
    path: '/auth/login',
    errorMessage: null,
    olderInput: { email: '', password: '' },
    errorHandler: [],
  });
};
exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).render('../views/auth/login.ejs', {
      path: '/login',
      pageTitle: 'Login',
      errorMessage: errors.array()[0].msg,
      olderInput: { email: email, password: password },
      errorHandler: errors.array(),
    });
  }

  User.findByEmail(email)
    .then((user) => {
      if (!user) {
        return res.redirect('/auth/login');
      }
      bcrypt.compare(password, user.password).then((doMatch) => {
        if (doMatch) {
          req.session.isLoggedIn = true;
          req.session.user = user;
        }
        res.redirect('/admin');
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getSignup = (req, res, next) => {
  res.render('../views/auth/signup.ejs', {
    pageTitle: 'Signup',
    path: '/auth/signup',
    errorMessage: null,
    olderInput: { email: '', password: '', confirmPassword: '' },
    errorHandler: [],
  });
};

exports.postSignup = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;
  const errors = validationResult(req);
  console.log(errors.array()[0]);
  if (!errors.isEmpty()) {
    return res.status(422).render('../views/auth/signup.ejs', {
      pageTitle: 'Signup',
      path: '/auth/signup',
      errorMessage: errors.array()[0].msg,
      olderInput: {
        email: email,
        password: password,
        confirmPassword: confirmPassword,
      },
      errorHandler: errors.array(),
    });
  }
  bcrypt
    .hash(password, 12)
    .then((hashedPassword) => {
      const user = new User(email, hashedPassword);
      user.saveUser();
      console.log('New user saved');
      res.redirect('/auth/login');
    })
    .catch((err) => {
      console.log(err);
    });
};

// Logging out and destroying the session
exports.logout = (req, res, next) => {
  req.session.destroy((err) => {
    console.log(err);
    res.redirect('/admin');
  });
};
