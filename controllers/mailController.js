const sgMail = require('@sendgrid/mail');

exports.postMail = (req, res, next) => {
  const name = req.body.name;
  const email = req.body.email;
  const subject = req.body.subject;
  const message = req.body.message;
  sgMail.setApiKey(`${process.env.SENDGRID_API_KEY}`);
  const msg = {
    // To can be any email as well
    to: 'pngodup123@gmail.com',
    from: email,
    subject: subject,
    text: message,
  };

  sgMail
    .send(msg)
    .then((response) => {
      console.log('email sent');
      console.log(response);
      res.redirect('/');
    })
    .catch((err) => {
      console.log(err);
    });
};
