## Readme for my personal portfolio project

### Setting up for the first time

In the beginning all the database will be empty. So, need to go to the profile and click name. Click there and we get to the login page. There need to login with the credential and we get to the admin panel.

**Database Info:**
_Need to create config.env file to have the PORT number, URI Key and SENDGRID_API_KEY._

**First Time View**

1. Database will be empty
2. Home page will have no data

**Adding Data To The Database**

- Go to the profile
- Right click on "Name"
- Click on the navigation bar as wished to stored data
- And press "save" for the first time

**About section:**
_About section data once stored to the database than, to edit the next time we can go the same page and all the previous data will get prompt and we can idit on the same page and post it._

**Experiences section:**
_In expereence section, while adding in job details. We need to seperate the end with full stop so it comes out in bullet form._

**Update the Differnet Section**

- Go to the update navigation in admin page
- All the data with delete and edit button will be available
- Click edit and it will generate to the corresponding page with all the details
- Do the editing and press the update button

**Delete the Differnet Section**

- Go to the update navigation in admin page
- All the data with delete and edit button will be available
- Click delete button and it will delete the respective data without any further process.
