const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
let _db;

const mongoConnect = (callback) => {
  MongoClient.connect(
    `mongodb+srv://${process.env.MONGO_NAME}:${process.env.MONGO_PASSWORD}@cluster0.9wgye.mongodb.net/${process.env.MONGO_DATABASE_NAME}`
  )
    .then((client) => {
      console.log('connected to the mongo database....');
      _db = client.db();
      callback();
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

const getDb = () => {
  if (_db) {
    return _db;
  } else {
    throw 'No database found';
  }
};

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;
