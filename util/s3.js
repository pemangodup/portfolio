const fs = require('fs');
const aws = require('aws-sdk');
var multer = require('multer');
var multerS3 = require('multer-s3');

const bucketName = process.env.AWS_BUCKET_NAME;
const region = process.env.AWS_BUCKET_REGION;
const accessKeyId = process.env.AWS_ACCESS_KEY;
const secretAccessKey = process.env.AWS_SECRET_KEY;

const s3 = new aws.S3({
  region,
  accessKeyId,
  secretAccessKey,
});

// upload a file to s3
exports.uploadFile = async (file) => {
  console.log(file);
  const fileStream = fs.createReadStream(file.path);

  const uploadParams = {
    Bucket: bucketName,
    Key: 'pemangodup-img',
    Body: fileStream,
  };

  return s3.upload(uploadParams).promise();
};

// delete a file from s3
exports.deleteFile = (fileKey) => {
  const deleteParams = {
    Bucket: bucketName,
    Key: fileKey,
  };
  s3.deleteObject(deleteParams).promise();
};
