const express = require('express');
const router = express.Router();
const { check, body } = require('express-validator');

const {
  getLogin,
  postLogin,
  getSignup,
  postSignup,
  logout,
} = require('../controllers/authController');
const User = require('../model/user');

router.route('/login').get(getLogin);
router.post(
  '/login',
  [
    body('email').isEmail().withMessage('Please enter correct email.'),
    body(
      'password',
      'Please enter  a password with number and text of at least 5 charcters.'
    )
      .isLength({ min: 5 })
      .isAlphanumeric()
      .trim(),
  ],
  postLogin
);
// router.route('/signup').get(getSignup);
// router.post(
//   '/signup',
//   [
//     check('email')
//       .isEmail()
//       .withMessage('Please enter a valid email id.')
//       .custom((value, { req }) => {
//         return User.findByEmail(value).then((userDoc) => {
//           if (userDoc) {
//             return Promise.reject(
//               'Email already exist please pick different one.'
//             );
//           }
//         });
//       }),
//     body(
//       'password',
//       'Please enter  a password with number and text and at least 5 charcters.'
//     )
//       .isLength({ min: 5 })
//       .isAlphanumeric(),
//     body('confirmPassword').custom((value, { req }) => {
//       if (value !== req.body.password) {
//         throw new Error('Password does not match with confirm password.');
//       }
//       return true;
//     }),
//   ],
//   postSignup
// );
router.route('/logout').get(logout);

module.exports = router;
