const express = require('express');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const isAuth = require('../middleware/is-auth');

const {
  getIndex,
  getAbout,
  saveAbout,
  getImagePage,
  postImage,
  getEducation,
  saveEducation,
  getExperiences,
  postExperiences,
  getEditExperiences,
  postEditExperiences,
  deleteExperiences,
  getUpdate,
  getUpdateEducation,
  postEditEducation,
  deleteEducation,
  logout,
} = require('../controllers/admin-controller');

router.route('/').get(getIndex);
router.route('/about').get(isAuth).get(getAbout).post(saveAbout);
router.route('/update').get(isAuth).post(saveAbout);
router.route('/uploadImg').get(isAuth).get(getImagePage);
router.route('/education').get(isAuth).get(getEducation).post(saveEducation);
router
  .route('/experiences')
  .get(isAuth)
  .get(getExperiences)
  .post(postExperiences);
router.route('/getEditExperiences/:id').get(isAuth).get(getEditExperiences);
router.route('/postEditExperiences').get(isAuth).post(postEditExperiences);
router.route('/deleteExperiences/:id').post(deleteExperiences);
router.route('/update/all').get(isAuth).get(getUpdate);
router.route('/getEditUpdate/:id').get(isAuth).get(getUpdateEducation);
router.route('/postEditEducation').get(isAuth).post(postEditEducation);
router.route('/deleteEducation').get(isAuth).post(deleteEducation);
router.route('/logout').get(logout);
// now for image uploading process
// var storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, 'public/img');
//   },
//   filename: function (req, file, cb) {
//     cb(null, file.fieldname + path.extname(file.originalname));
//   },
// });

// var upload = multer({
//   storage: storage,
// });
//router.route('/uploadImage', upload.single('profileImg')).post(postImage);

const upload = multer({ dest: 'public/' });
router.post('/uploadImage', upload.single('profileImg'), postImage);

module.exports = router;
