const express = require('express');
const router = express.Router();

const { postMail } = require('../controllers/mailController');
router.route('/send').post(postMail);

module.exports = router;
